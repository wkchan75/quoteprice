package quoteprice;

public interface ReferencePriceSource {

	void subscribe(ReferencePriceSourceListener listener);

    double get(int securityId);
    
    void set(int securityId, double value);
}
