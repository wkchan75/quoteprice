package quoteprice;

import quoteprice.field.Field;
import java.lang.Integer;

public class IntField extends Field<Integer> {
	
	static final long serialVersionUID = 2987365037924483087L;

	public IntField(int field) {
        super(field, 0);
    }

    public IntField(int field, Integer data) {
        super(field, data);
    }

    public IntField(int field, int data) {
        super(field, data);
    }

    public void setValue(Integer value) {
        setObject(value);
    }

    public void setValue(int value) {
        setObject(value);
    }

    public int getValue() {
        return getObject();
    }

    public boolean valueEquals(Integer value) {
        return getObject().equals(value);
    }

    public boolean valueEquals(int value) {
        return getObject().equals(value);
    }
}
