package quoteprice;

import quoteprice.field.*;

public class DoubleField extends Field<Double> {
	
	static final long serialVersionUID = -9085888167552347030L;
	
	private int padding = 0;

    public DoubleField(int field) {
        super(field, 0d);
    }

    public DoubleField(int field, Double data) {
        super(field, data);
        checkForValidDouble(data);
    }

    public DoubleField(int field, double data) {
        super(field, data);
        checkForValidDouble(data);
    }

    public DoubleField(int field, double data, int padding) {
        super(field, data);
        checkForValidDouble(data);
        this.padding = padding;
    }

    public void setValue(Double value) {
        checkForValidDouble(value);
        setObject(value);
    }

    public void setValue(double value) {
        checkForValidDouble(value);
        setObject(value);
    }

    public double getValue() {
        return getObject();
    }

    public int getPadding() {
        return padding;
    }

    public boolean valueEquals(Double value) {
        return getObject().equals(value);
    }

    public boolean valueEquals(double value) {
        return getObject().equals(value);
    }

    private void checkForValidDouble(Double value) throws NumberFormatException {
        if (Double.isInfinite(value) || Double.isNaN(value)) {
            throw new NumberFormatException("Tried to set NaN or infinite value.");
        }
    }
}
