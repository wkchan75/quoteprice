package quoteprice;

public class QuoteCalculationEngine implements PricingEngine {

	public QuoteCalculationEngine() {
	}

	@Override
	public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
		//I should be using BSE model to price the instrument. However due to time constrainst. I will multiple the price with constant vol.
		return (referencePrice * quantity * 0.02) / quantity + referencePrice;
	}
}
