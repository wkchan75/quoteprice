package quoteprice;

import quoteprice.exception.ConfigError;

public interface Acceptor {
	String SOCKET_ACCEPT_PORT 		= "SocketAcceptPort";
	String SOCKET_ACCEPT_ADDRESS 	= "SocketAcceptAddress";
	String SOCKET_ACCEPT_PROTOCOL   = "SocketAcceptProtocol";
	
	void start() throws ConfigError;
	void stop(boolean force);
}
