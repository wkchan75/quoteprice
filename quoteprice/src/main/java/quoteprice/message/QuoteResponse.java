package quoteprice.message;

import java.math.BigDecimal;

import quoteprice.Message;
import quoteprice.exception.FieldException;
import quoteprice.exception.FieldNotFound;
import quoteprice.exception.InvalidMessage;
import quoteprice.field.Price;
import quoteprice.field.Qty;
import quoteprice.field.SecId;
import quoteprice.field.Side;
import quoteprice.field.converter.DoubleConverter;

public class QuoteResponse extends Message {
	
	private static final long serialVersionUID = -1526837926097844843L;

	public static String MSG_TYPE	= "QuoteResponse";
	
	public QuoteResponse(Price quotedPrice) {
		super();
		setField(quotedPrice);
	}

	public void setFieldOrder() {
		int [] fieldSeq = new int[1];
		fieldSeq[0] = Price.FIELD;
		super.setFieldOrder(fieldSeq);
    }
	
	@Override
	public void parse(String messageData) throws InvalidMessage, FieldNotFound {
		Double value = Double.valueOf(messageData);
		if (messageData.isEmpty()) {
			getField(44).setValue(value.toString(value.doubleValue()));
		}
	}
	
	public double getPrice() throws FieldNotFound, FieldException {		
		return getDouble(44);
	}
}
