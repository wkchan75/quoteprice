package quoteprice.message;

import quoteprice.Message;
import quoteprice.StringField;
import quoteprice.exception.FieldException;
import quoteprice.exception.FieldNotFound;
import quoteprice.exception.InvalidMessage;
import quoteprice.field.Qty;
import quoteprice.field.SecId;
import quoteprice.field.Side;

public class QuoteRequest extends Message {
	
	private static final long serialVersionUID = 5229019847452602802L;

	public static String MSG_TYPE	= "QuoteRequest";
	
	public QuoteRequest(SecId securityID, Side direction, Qty quantity) {
		
		setField(securityID);
		setField(direction);
		setField(quantity);
	}
	
	public void setFieldOrder() {
		int [] fieldSeq = new int[3];
		fieldSeq[0] = SecId.FIELD;
		fieldSeq[1] = Side.FIELD;
		fieldSeq[2] = Qty.FIELD;
		super.setFieldOrder(fieldSeq);
    }
	
	public void parse(String messageData) throws InvalidMessage, FieldNotFound{
		int offset = messageData.indexOf(' ', 0);
		int securityId = Integer.parseInt(messageData.substring(0, offset));
		if (securityId > 0) {
			getField(48).setValue(Integer.toString(securityId));
		}
		
		int fromOffset = offset;
		offset = messageData.indexOf(' ', fromOffset+1);
		String side = messageData.substring(fromOffset+1, offset);
		getField(54).setValue(side);
		
		fromOffset = offset;
		offset = messageData.indexOf(' ', fromOffset+1);
		int qty = Integer.parseInt(messageData.substring(fromOffset+1));
		if (qty >= 0) {
			getField(53).setValue(Integer.toString(qty));
		}
	}
	
	public int getSecIdID() throws FieldNotFound, FieldException {		
		return getInt(48);
	}
	
	public String getSide() throws FieldNotFound, FieldException {		
		return getString(54);
	}

	public int getQty() throws FieldNotFound, FieldException {		
		return getInt(53);
	}
}
