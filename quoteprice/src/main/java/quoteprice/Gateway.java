package quoteprice;

public interface Gateway {
	void onQuoteRequest(Message message);
	void sendQuote(Message message);
}
