package quoteprice.field;

import java.io.Serializable;

public class Field<T> implements Serializable {	
	static final long serialVersionUID = -2146308201931856115L;

	private int id;
    private T object;
    
    public Field(int field, T object) {
        this.id = field;
        this.object = object;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    protected void setObject(T object) {
        this.object = object;
    }
    
    public T getObject() {
        return object;
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder("");
        buffer.append(id).append('=').append(object.toString());
    	return buffer.toString();
    }
    
    public int hashCode() {
        return object.hashCode();
    }
    
    public boolean equals(Object object) {
        return  object instanceof Field
                   && id == ((Field<?>) object).getId()
                   && getObject().equals(((Field<?>) object).getObject());
    }
}
