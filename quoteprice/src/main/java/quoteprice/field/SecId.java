package quoteprice.field;

import quoteprice.IntField;

public class SecId extends IntField {
	
	private static final long serialVersionUID = 5186689220504776959L;

	public static final int FIELD = 48;
	
	public SecId() {
		super(48);
	}

	public SecId(int data) {
		super(48, data);
	}
}
