package quoteprice.field;

import quoteprice.StringField;

public class Side extends StringField {

	private static final long serialVersionUID = 5581149575188901234L;

	public static final int FIELD = 54;
	
	public Side() {
		super(54);
	}

	public Side(String data) {
		super(54, data);
	}
	
}
