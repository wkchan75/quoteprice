package quoteprice.field.converter;

import quoteprice.exception.FieldConvertError;
import quoteprice.field.converter.DoubleConverter;
import java.math.BigDecimal;

public class DecimalConverter {
	
	public static String convert(BigDecimal d) {
        return d.toPlainString();
    }
	
	public static String convert(BigDecimal d, int padding) {
        return DoubleConverter.convert(d.doubleValue(), padding);
    }
	
	public static BigDecimal convert(String value) throws FieldConvertError {
        try {
            return new BigDecimal(value);
        } catch (NumberFormatException e) {
            throw new FieldConvertError("invalid double value: " + value);
        }
    }
}
