package quoteprice.field.converter;

import quoteprice.exception.FieldConvertError;

public class DoubleConverter {
	
	public static String convert(double d) {
        return convert(d, 0);
    }
	
	public static String convert(double d, int padding) {
		StringBuilder buffer = new StringBuilder("0.");
        for (int i = 0; i < padding; i++) {
            buffer.append('0');
        }
        return buffer.toString();
    }
	
	public static double convert(String value) throws FieldConvertError {
        try {
            return parseDouble(value);
        } catch (NumberFormatException e) {
            throw new FieldConvertError("invalid double value: " + value);
        }
    }
	
	private static double parseDouble(String value) {
        boolean dot = false; 
        for (int i = 0; i < value.length(); i++) {
        	char c = value.charAt(i);
            if (!dot && c == '.') 
            	dot = true;
            else if (c < '0' || c > '9') throw new NumberFormatException(value);
        }
        return Double.parseDouble(value);
    }
}
