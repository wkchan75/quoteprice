package quoteprice.field.converter;

import quoteprice.exception.FieldConvertError;

public class BooleanConverter {
	private static final String NO = "N";
    private static final String YES = "Y";
    
    public static String convert(boolean b) {
        return b ? YES : NO;
    }
    
    public static boolean convert(String value) throws FieldConvertError {
        if (YES.equals(value)) {
            return true;
        } else if (NO.equals(value)) {
            return false;
        } else {
            throw new FieldConvertError("invalid boolean value: " + value);
        }
    }
}
