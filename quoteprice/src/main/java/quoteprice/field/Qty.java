package quoteprice.field;

import quoteprice.IntField;

public class Qty extends IntField {

	private static final long serialVersionUID = 682072550527656635L;

	public static final int FIELD = 53;
	
	public Qty() {
		super(53);
	}

	public Qty(int data) {
		super(53, data);
	}
}
