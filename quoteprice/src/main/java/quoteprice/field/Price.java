package quoteprice.field;

import quoteprice.DoubleField;

public class Price extends DoubleField {
	
	private static final long serialVersionUID = 2445742077172692159L;
	
	public static final int FIELD = 44;
	
	public Price() {
		super(44);
	}

	public Price(double data) {
		super(44, data);
	}
}
