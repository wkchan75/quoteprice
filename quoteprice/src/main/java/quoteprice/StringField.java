package quoteprice;

import quoteprice.field.*;

public class StringField extends Field<String> {
		
	static final long serialVersionUID = -6689266842456118799L;

	public StringField(int field) {
        super(field, "");
    }

    public StringField(int field, String data) {
        super(field, data);
    }

    public void setValue(String value) {
        setObject(value);
    }

    public String getValue() {
        return getObject();
    }

    public boolean valueEquals(String value) {
        return getValue().equals(value);
    }
}
