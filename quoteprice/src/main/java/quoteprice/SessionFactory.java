package quoteprice;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quoteprice.mina.Session;

public class SessionFactory {
	protected final Logger log = LoggerFactory.getLogger(getClass());
	private static final ConcurrentMap<String, Session> sessions = new ConcurrentHashMap<>();
	private Gateway manager;
	
	public SessionFactory(Gateway quoteGateway){
		this.manager = quoteGateway;
	}
	
	public Session createSession(String sessionID) {
		Session session = new Session(manager, sessionID);
		registerSession(session);
		return session;
	}
	
	void registerSession(Session session) {
		if (sessions.get(session.getSessionID()) == null) {
			sessions.put(session.getSessionID(), session);
		}
		else {
			log.error("Session already exists! id:{}" + session.getSessionID());
		}
    }
	
	public void unregisterAllSessions() {
		List<Session> list = new ArrayList<Session>(sessions.values());
        for (Session session : list) {
        	session.unregisterSession();
        }
    }
	
	void unregisterSession(String sessionId) {
		Session session = lookupSession(sessionId);
		if (session != null) {
			session.unregisterSession();
			sessions.remove(sessionId);
		}
    }
	
	public Session lookupSession(String sessionId){
		if (sessions.get(sessionId) != null) {
			return sessions.get(sessionId);
		}
		return null;
	}
}
