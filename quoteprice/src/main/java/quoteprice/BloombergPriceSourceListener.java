package quoteprice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BloombergPriceSourceListener implements ReferencePriceSourceListener {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	ReferencePriceSource source;
	
	@Override
	public void referencePriceChanged(int securityId, double price) {
		log.info("Update reference price for SecurityID: {}, Reference Price:{}", securityId, price);
		source.set(securityId, price);
	}

	@Override
	public void doSubscription(ReferencePriceSource source) {
		log.info("Successfully subscribe to BloombergPriceSourceListener!");
		this.source = source;
		
		Thread workker = new Thread(new RunnableWrapper());
		workker.start();
	}

	private class RunnableWrapper implements Runnable { 
		  
        public void run() 
        { 
        	referencePriceChanged(1234, 1.5);
        	referencePriceChanged(4567, 90.11);
        	referencePriceChanged(1000, 12.05);
        } 
    } 
	
}
