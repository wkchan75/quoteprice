package quoteprice.mina;

import org.apache.mina.core.service.IoService;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Properties;

import quoteprice.exception.FieldConvertError;
import quoteprice.field.converter.*;

public class NetOptions {

	private final Logger log = LoggerFactory.getLogger(getClass());
	private final Boolean keepAlive;
	private final Boolean tcpNoDelay;
	
	public static final String SETTING_SOCKET_KEEPALIVE = "SocketKeepAlive";
	public static final String SETTING_SOCKET_TCP_NODELAY = "SocketTcpNoDelay";
	
	public NetOptions(Properties properties) throws FieldConvertError {
		
		keepAlive = getBoolean(properties, SETTING_SOCKET_KEEPALIVE, null);
		tcpNoDelay = getBoolean(properties, SETTING_SOCKET_TCP_NODELAY, Boolean.TRUE);
	}
	
	private Boolean getBoolean(Properties properties, String key, Boolean defaultValue) throws FieldConvertError {
        Boolean value = properties.containsKey(key) ? Boolean.valueOf(BooleanConverter.convert(properties.getProperty(key))) : defaultValue;
        log.info("Socket option: {}={}", key, value);
        return value;
    }
	
	public void apply(IoService service) {
		IoSessionConfig sessionConfig = service.getSessionConfig();
		if (sessionConfig instanceof SocketSessionConfig) {
			SocketSessionConfig socketSessionConfig = (SocketSessionConfig) sessionConfig;
			if (keepAlive != null) {
                socketSessionConfig.setKeepAlive(keepAlive);
            }
			if (tcpNoDelay != null) {
                socketSessionConfig.setTcpNoDelay(tcpNoDelay);
            }
		}
	}
}
