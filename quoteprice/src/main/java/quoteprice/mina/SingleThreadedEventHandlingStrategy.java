package quoteprice.mina;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.CountDownLatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quoteprice.Gateway;
import quoteprice.Message;

public class SingleThreadedEventHandlingStrategy implements EventHandlingStrategy {
	protected final Logger log = LoggerFactory.getLogger(getClass());
	public static final String MESSAGE_PROCESSOR_NAME = "Message Processor";
	private static final int MAX_COUNT = 1000000;
	private final BlockingQueue<MessageEvent> eventQueue;
	private volatile ThreadAdapter processingThread;
	private volatile boolean isStopped;
    
    private class MessageEvent {
        private final Message message;

        public MessageEvent(Message message) {
            this.message = message;
        }

        public void processMessage() {
            try {
            	Session session = message.getSession();
            	Gateway gateway = session.getManager();
            	gateway.onQuoteRequest(message);
            } catch (Throwable e) {
            	log.error(e.getMessage());
            }
        }
    }
    
    final class ThreadAdapter {

		private final Executor executor;
		private final RunnableWrapper wrapper;

		ThreadAdapter(Runnable command, String name) {
			wrapper = new RunnableWrapper(command, name);
			this.executor = new DefaultThreadExecutor(name);
		}

		public void join() throws InterruptedException {
			wrapper.join();
		}

		public boolean isAlive() {
			return wrapper.isAlive();
		}

		public void start() {
			executor.execute(wrapper);
		}
		
		final class RunnableWrapper implements Runnable {

			private final CountDownLatch latch = new CountDownLatch(1);
			private final Runnable command;
			private final String name;

			public RunnableWrapper(Runnable command, String name) {
            	this.command = command;
                this.name = name;
			}

            @Override
            public void run() {
                Thread current = Thread.currentThread();
                String threadName = current.getName();
                try {
                    if (!name.equals(threadName)) {
                    	current.setName(name + " (" + threadName + ")");
                    }
                    command.run();
                } finally {
                    latch.countDown();
                    current.setName(threadName);
                }
            }

			public void join() throws InterruptedException {
            	latch.await();
			}

			public boolean isAlive() {
				return latch.getCount() > 0;
			}
		}
    }
    
    final class DefaultThreadExecutor implements Executor {

		private final String name;
        private Thread thread;
		
        DefaultThreadExecutor(String name) {
			this.name = name;
		}

		@Override
		public void execute(Runnable command) {
			thread = new Thread(command, name);
			thread.setDaemon(true);
			thread.start();
		}
	}
    
    public SingleThreadedEventHandlingStrategy(int capacity) {
        eventQueue = new LinkedBlockingQueue<>((capacity<0)? MAX_COUNT: capacity);
        isStopped = false;
    }
    
    @Override
    public void onMessage(Message message) {
        if (isStopped) {
            return;
        }
        try {
        	eventQueue.put(new MessageEvent(message));
        } catch (InterruptedException e) {
            isStopped = true;
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public int getQueueSize() {
        return eventQueue.size();
    }
    
    private MessageEvent getMessage() throws InterruptedException {
        return eventQueue.poll(MESSAGE_WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }
    
    private void runLoop() {
        while (true) {
            synchronized (this) {
                if (isStopped) {
                    if (!eventQueue.isEmpty()) {
                        final List<MessageEvent> tempList = new ArrayList<>(eventQueue.size());
                        eventQueue.drainTo(tempList);
                        for (MessageEvent event : tempList) {
                            event.processMessage();
                        }
                    }
                    return;
                }
            }
            try {
                MessageEvent event = getMessage();
                if (event != null) {
                    event.processMessage();
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    @Override
    public void startHandling() {        
        isStopped = false;
        processingThread = new ThreadAdapter(new Runnable() {
			@Override
			public void run() {
				log.info("Started {} thread", MESSAGE_PROCESSOR_NAME);
			    runLoop();
			    log.info("Stopped {}", MESSAGE_PROCESSOR_NAME);
			}
		}, MESSAGE_PROCESSOR_NAME);
        processingThread.start();
    }
    
    public synchronized void stopHandling() {
        isStopped = true;
    }
    
    @Override
    public void stopHandling(boolean join) {
    	stopHandling();
        if (join) {
            try {
                processingThread.join();
                log.info("{} has been stopped.", MESSAGE_PROCESSOR_NAME);
            } catch (InterruptedException e) {
                log.error("{} interrupted.", MESSAGE_PROCESSOR_NAME);
            }
        }
    }
}
