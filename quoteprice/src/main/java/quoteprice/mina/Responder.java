package quoteprice.mina;

public interface Responder {

	boolean send(String data);
	void disconnect();
	String getRemoteAddress();
}
