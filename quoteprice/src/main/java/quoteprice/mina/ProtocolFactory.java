package quoteprice.mina;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import quoteprice.exception.ConfigError;
import quoteprice.exception.RuntimeError;

public class ProtocolFactory {

    public final static int SOCKET = 0;

    public static String getTypeString(int type) {
        switch (type) {
        case SOCKET:
            return "SOCKET";        
        default:
            return "unknown";
        }
    }

    public static SocketAddress createSocketAddress(int transportType, String host, int port) throws ConfigError {
        if (transportType == SOCKET) {
            return host != null ? new InetSocketAddress(host, port) : new InetSocketAddress(port);        
        } else {
            throw new ConfigError("Unknown session transport type: " + transportType);
        }
    }

    public static int getAddressTransportType(SocketAddress address) {
        if (address instanceof InetSocketAddress) {
            return SOCKET;
        } else {
            throw new RuntimeError("Unknown address type: " + address.getClass().getName());
        }
    }

    public static int getTransportType(String string) {
        if (string.equalsIgnoreCase("tcp") || string.equalsIgnoreCase("SOCKET")) {
            return SOCKET;
        } else {
            throw new RuntimeError("Unknown Transport Type type: " + string);
        }
    }

    public static IoAcceptor createIoAcceptor(int transportType) {
        if (transportType == SOCKET) {
            NioSocketAcceptor ret = new NioSocketAcceptor();
            ret.setReuseAddress(true);
            return ret;
        } else {
            throw new RuntimeError("Unsupported transport type: " + transportType);
        }
    }
    
}

