package quoteprice.mina.acceptor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecException;
import org.apache.mina.filter.codec.ProtocolDecoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quoteprice.Message;
import quoteprice.MessageFactory;
import quoteprice.exception.InvalidMessage;
import quoteprice.message.QuoteRequest;
import quoteprice.mina.EventHandlingStrategy;
import quoteprice.mina.NetOptions;

public abstract class AbstractIoHandler extends IoHandlerAdapter {
	protected final Logger log = LoggerFactory.getLogger(getClass());
	private final NetOptions options;
	private final EventHandlingStrategy eventHandlingStrategy;
	
	public AbstractIoHandler(NetOptions options, EventHandlingStrategy eventHandlingStrategy) {
		this.options = options;
        this.eventHandlingStrategy = eventHandlingStrategy;
    }
	
	@Override
    public void exceptionCaught(IoSession ioSession, Throwable cause) throws Exception {
        boolean disconnectNeeded = false;
        Throwable realCause = cause;
        if (cause instanceof ProtocolDecoderException && cause.getCause() != null) {
            realCause = cause.getCause();
        } else {
            Throwable chain = cause;
            while (chain != null && chain.getCause() != null) {
                chain = chain.getCause();
                if (chain instanceof IOException) {
                    realCause = chain;
                    break;
                }
            }
        }
        String reason;
        if (realCause instanceof IOException) {
        	reason = "Socket exception (" + ioSession.getRemoteAddress() + "): " + cause;
            disconnectNeeded = true;
        } else if (realCause instanceof ProtocolCodecException) {
            reason = "Protocol handler exception: " + cause;
        } else {
            reason = cause.toString();
        }
        if (disconnectNeeded) {
            try {
                
            	log.error(reason, cause);
                ioSession.closeNow();
            } finally {
            }
        } else {
                log.error(reason, cause);
        }
    }
	
	protected abstract void processMessage(IoSession ioSession, Message message) throws Exception;
	
	@Override
    public void sessionClosed(IoSession ioSession) {
		ioSession.closeNow();
		/*
		try {
			eventHandlingStrategy.onMessage(EventHandlingStrategy.STOP);
        } catch (Exception e) {
            throw e;
        } finally {
            ioSession.closeNow();
        }*/
	}
	
	protected NetOptions getNetOptions() {
        return options;
    }
	
	@Override
    public void messageReceived(IoSession ioSession, Object message) throws Exception {
		IoBuffer buff = (IoBuffer)message;
		byte[] data = new byte[buff.limit()];
        buff.get(data);
        String messageString = new String(data);
        
        log.info("Receiving data from client.length:{} {}", messageString.length(), messageString);
        try {
            Message msg = MessageFactory.create(QuoteRequest.MSG_TYPE);
            if (msg != null) {
            	msg.parse(messageString);
            	processMessage(ioSession, msg);
            }
        } catch (InvalidMessage e) {
            final Message msg = e.getQuoteMessage();
            log.info("Invalid Message! {}", e.getMessage());
            ioSession.closeNow();
        }
    }
}
