package quoteprice.mina.acceptor;

import java.net.SocketAddress;

import org.apache.mina.core.session.IoSession;
import quoteprice.Message;
import quoteprice.SessionFactory;
import quoteprice.mina.EventHandlingStrategy;
import quoteprice.mina.IoSessionResponder;
import quoteprice.mina.NetOptions;
import quoteprice.mina.Session;

public class AcceptorIoHandler extends AbstractIoHandler{
	
	private final EventHandlingStrategy eventHandlingStrategy;
	private final SessionFactory		sessionFactory;
	
	public AcceptorIoHandler(NetOptions options, EventHandlingStrategy eventHandlingStrategy, SessionFactory sessionFactory) {
        super(options, eventHandlingStrategy);
        this.eventHandlingStrategy = eventHandlingStrategy;
        this.sessionFactory = sessionFactory;
    }

	@Override
    public void sessionCreated(IoSession ioSession) throws Exception {
        super.sessionCreated(ioSession);
        log.info("MINA session created: local={}, {}, remote={}", ioSession.getLocalAddress(), ioSession.getClass(), ioSession.getRemoteAddress());
        String sessionId = getSessionID(ioSession);
        Session session = sessionFactory.createSession(sessionId);
        session.setResponder(new IoSessionResponder(ioSession, true, 200));
        session.setIoSession(ioSession);
    }
	
	String getSessionID(IoSession ioSession){
		SocketAddress remoteAddr = ioSession.getRemoteAddress();
        return remoteAddr.toString();
	}
	
	@Override
    public void sessionClosed(IoSession ioSession) {
		log.info("MINA session close: remote={}", ioSession.getRemoteAddress());
		sessionFactory.unregisterAllSessions();
		super.sessionClosed(ioSession);
	}
	
	@Override
    protected void processMessage(IoSession ioSession, Message message) throws Exception {
		String sessionId = getSessionID(ioSession);
		Session session = sessionFactory.lookupSession(sessionId);
		if (session != null) {
			message.setSession(session);
	        eventHandlingStrategy.onMessage(message);
		}
    }
}
