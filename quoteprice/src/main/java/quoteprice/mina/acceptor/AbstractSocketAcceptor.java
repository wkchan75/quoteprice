package quoteprice.mina.acceptor;

import quoteprice.Acceptor;
import quoteprice.SessionFactory;
import quoteprice.exception.*;
import quoteprice.mina.EventHandlingStrategy;
import quoteprice.mina.NetOptions;
import quoteprice.mina.ProtocolFactory;
import quoteprice.field.converter.*;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.buffer.SimpleBufferAllocator;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;



public abstract class AbstractSocketAcceptor implements Acceptor {

	protected final Logger log = LoggerFactory.getLogger(getClass());
	protected final Properties settings;
	protected final SessionFactory sessionFactory;
	private final Map<SocketAddress, AcceptorSocketDescriptor> socketDescriptorForAddress = new HashMap<>();
	private final ConcurrentMap<AcceptorSocketDescriptor, IoAcceptor> ioAcceptors = new ConcurrentHashMap<>();
	
	
	private static class AcceptorSocketDescriptor {
		private final SocketAddress address;
		
		public AcceptorSocketDescriptor(SocketAddress address) {
            this.address = address;
		}
		public SocketAddress getAddress() {
            return address;
        }
	}
	
	protected AbstractSocketAcceptor(Properties settings, SessionFactory sessionFactory) throws ConfigError {
		IoBuffer.setAllocator(new SimpleBufferAllocator());
        IoBuffer.setUseDirectBuffer(false);
		this.settings = settings;
		this.sessionFactory = sessionFactory;
	}
	
	protected abstract EventHandlingStrategy getEventHandlingStrategy() ;
	
    protected void startAcceptingConnections() throws ConfigError {
    	//IoBuffer buf;buf.putString
    	SocketAddress address = null;
        try {
        	String acceptHost = settings.getProperty(SOCKET_ACCEPT_ADDRESS);
        	int acceptPort = (int)Long.parseLong(settings.getProperty(SOCKET_ACCEPT_PORT));
        	int acceptTransportType = ProtocolFactory.getTransportType(settings.getProperty(SOCKET_ACCEPT_PROTOCOL));
        	
        	SocketAddress acceptorAddress = ProtocolFactory.createSocketAddress(
        										acceptTransportType,
        										acceptHost, 
        										acceptPort);

        	AcceptorSocketDescriptor descriptor = socketDescriptorForAddress.get(acceptorAddress);
        	if (descriptor == null) {
        		socketDescriptorForAddress.put(acceptorAddress, new AcceptorSocketDescriptor(acceptorAddress));
	            for (AcceptorSocketDescriptor socketDescriptor : socketDescriptorForAddress.values()) {
	                address = socketDescriptor.getAddress();
	                IoAcceptor ioAcceptor = getIoAcceptor(socketDescriptor);   
	                
	                /*ioAcceptor.getFilterChain().addLast("logger", new LoggingFilter());  
	                  ioAcceptor.getFilterChain().addLast(
	                		"codec",
	                        new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("ISO-8859-1"))));
					*/	
                	ioAcceptor.setCloseOnDeactivation(false);
	                ioAcceptor.bind(socketDescriptor.getAddress());
	                log.info("Listening for connections at {}", address);
	            }
        	}
            
        } catch (Exception e) {
            log.error("Cannot start acceptor session for {}, error: {}", address, e);
            throw new RuntimeError(e);
        }
    }
    
    protected void stopAcceptingConnections() {
    	for (AcceptorSocketDescriptor socketDescriptor : socketDescriptorForAddress.values()) {
    		IoAcceptor ioAcceptor;
			try {
				ioAcceptor = getIoAcceptor(socketDescriptor);
				ioAcceptor.unbind();
			} catch (ConfigError e) {
				e.printStackTrace();
			}
    	}
    	sessionFactory.unregisterAllSessions();    	
    }
    
    private IoAcceptor getIoAcceptor(AcceptorSocketDescriptor socketDescriptor) throws ConfigError {
        int transportType = ProtocolFactory.getAddressTransportType(socketDescriptor.getAddress());

        IoAcceptor ioAcceptor = ioAcceptors.get(socketDescriptor);
        if (ioAcceptor == null) {
            ioAcceptor = ProtocolFactory.createIoAcceptor(transportType);
            try {
                NetOptions options = new NetOptions(settings);
                options.apply(ioAcceptor);
                ioAcceptor.setHandler(new AcceptorIoHandler(options, getEventHandlingStrategy(), sessionFactory));
                
            } catch (FieldConvertError e) {
                throw new ConfigError(e);
            }
            ioAcceptors.put(socketDescriptor, ioAcceptor);
        }
        return ioAcceptor;
    }
    
    
}
