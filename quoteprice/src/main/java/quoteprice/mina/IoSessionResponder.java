package quoteprice.mina;

import java.net.SocketAddress;
import java.nio.ByteBuffer;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IoSessionResponder implements Responder {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private final IoSession ioSession;
    private final boolean synchronousWrites;
    private final long synchronousWriteTimeout;

    public IoSessionResponder(IoSession session, boolean synchronousWrites, long synchronousWriteTimeout) {
        this.ioSession = session;
        this.synchronousWrites = synchronousWrites;
        this.synchronousWriteTimeout = synchronousWriteTimeout;
    }

	@Override
	public boolean send(String data) {
		byte[] bytes = data.getBytes();
		IoBuffer bf = IoBuffer.allocate(bytes.length);
		bf.put(bytes);
		bf.flip();
		WriteFuture future = ioSession.write(bf);
		
        if (synchronousWrites) {
            try {
                if (!future.awaitUninterruptibly(synchronousWriteTimeout)) {
                    log.error("Write timed out! timeout: {}ms", synchronousWriteTimeout);
                    return false;
                }
            } catch (RuntimeException e) {
                log.error("Write failed: {}", e.getMessage());
                return false;
            }
        }
        return true;
	}

	@Override
	public void disconnect() {
		ioSession.closeOnFlush();
	}

	@Override
	public String getRemoteAddress() {
		final SocketAddress remoteAddress = ioSession.getRemoteAddress();
        if (remoteAddress != null) {
            return remoteAddress.toString();
        }
		return null;
	}
	
	IoSession getIoSession() {
        return ioSession;
    }
}
