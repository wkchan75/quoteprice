package quoteprice.mina;

import quoteprice.Message;

public interface EventHandlingStrategy {

	long MESSAGE_WAIT_TIME_MS = 200;
	//Message STOP = new Message();
	
	void onMessage(Message message);
	int getQueueSize();
	void startHandling();
	void stopHandling(boolean join);
}
