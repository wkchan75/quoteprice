package quoteprice.mina;

import java.io.Closeable;
import java.io.IOException;

import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quoteprice.Gateway;
import quoteprice.Message;

public class Session implements Closeable {
	protected final Logger log = LoggerFactory.getLogger(getClass());	
	private Gateway manager;
	private String sessionID;
	private Responder responder = null;
	private IoSession ioSession = null;

	public Session(Gateway manager, String sessionID){
		this.manager = manager;
		this.sessionID = sessionID;
	}
		
	public void setIoSession(IoSession session) {
		this.ioSession = session;
	}
	
	public String getSessionID() {
        return sessionID;
    }
	
	public Gateway getManager() {
		return manager;
	}
	
	public void setResponder(Responder responder) {
        this.responder = responder;
    }
	
	public Responder getResponder() {
		return responder;
    }
	
	public String getRemoteAddress() {
        Responder responder = getResponder();
        if (responder != null) {
            return responder.getRemoteAddress();
        }
        return null;
    }
	
	public boolean send(Message message) {
    	String messageString = message.toString();
    	if (responder != null) {
    		return responder.send(messageString);
    	}
    	else {
    		log.error("No responder, send message failed: " + messageString);
    	}
        return false;
    }
	
	public void unregisterSession() {
    	try {
    		close();
    	} catch (final IOException e) {
    		log.error("Failed to close session resources", e);
    	}
    }
	
	@Override
	public void close() throws IOException {
		ioSession.closeNow();
	}
		
}
