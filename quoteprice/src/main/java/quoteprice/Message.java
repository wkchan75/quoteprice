package quoteprice;

import java.net.SocketAddress;

import quoteprice.exception.FieldNotFound;
import quoteprice.exception.InvalidMessage;
import quoteprice.mina.Session;

public class Message extends FieldMap {
	
	private static final long serialVersionUID = 5860112161663391734L;
	
	protected Session session;
	private int[] fieldOrder;
	
	public Message() {
		this.fieldOrder = null;
    }
	
	public void setFieldOrder(int[] fieldOrder) {
        this.fieldOrder = fieldOrder;
    }

	public String toString() {
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < fieldOrder.length; i++) {
			int id = fieldOrder[i];
			try {
				String data = getField(id).getValue();
				buff.append(data);
				buff.append(' ');
			} catch (FieldNotFound e) {
				e.printStackTrace();
			}
	    }
		return buff.toString();
	}
	
	public void setSession(Session session) {
		this.session = session;
	}
	
	public Session getSession() {
		return session;
	}
	
	private boolean setField(FieldMap fields, StringField field) {
        if (fields.isSetField(field.getId())) {
            return false;
        }
        fields.setField(field);
        return true;
    }
	
	public void parse(String messageString) throws InvalidMessage, FieldNotFound{
	}
}
