package quoteprice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map.Entry;
import java.util.*;

import quoteprice.exception.*;
import quoteprice.field.converter.DecimalConverter;
import quoteprice.field.converter.DoubleConverter;
import quoteprice.field.converter.IntConverter;
import quoteprice.field.Field;

public abstract class FieldMap implements Serializable {

	private static final long serialVersionUID = -444660672334015196L;

	protected final TreeMap<Integer, Field<?>> fields;
	
	protected FieldMap(int[] fieldOrder) {
        fields = new TreeMap<>();
    }
	
	protected FieldMap() {
		this(null);
    }
	
	public void clear() {
        fields.clear();
    }
	
	public boolean isEmpty() {
        return fields.isEmpty();
    }
	
	public void setFields(FieldMap fieldMap) {
        fields.clear();
        fields.putAll(fieldMap.fields);
    }
	
	public void setField(StringField field) {
        if (field.getValue() == null) {
            throw new NullPointerException("Null field values are not allowed.");
        }
        fields.put(field.getId(), field);
    }
	
	public void setField(IntField field) {
        setInt(field.getId(), field.getValue());
    }
	
	public void setField(DoubleField field) {
        setDouble(field.getId(), field.getValue());
    }
	
	public void setString(int field, String value) {
        setField(new StringField(field, value));
    }
	
	public void setInt(int field, int value) {
        setField(new StringField(field, IntConverter.convert(value)));
    }
	
	public void setDouble(int field, double value) {
        setDouble(field, value, 10);
    }
	
	public void setDouble(int field, double value, int padding) {
        setField(new StringField(field, DoubleConverter.convert(value, padding)));
    }
	
	public void setDecimal(int field, BigDecimal value) {
        setField(new StringField(field, DecimalConverter.convert(value)));
    }
	
	public StringField getField(int field) throws FieldNotFound {
        final StringField f = (StringField) fields.get(field);
        if (f == null) {
            throw new FieldNotFound(field);
        }
        return f;
    }
	
	public String getString(int field) throws FieldNotFound {
        return getField(field).getObject();
    }
	
	public int getInt(int field) throws FieldNotFound, FieldException {
        try {
            return IntConverter.convert(getString(field));
        } catch (final FieldConvertError e) {
            throw new FieldException(e.getMessage(), field);
        }
    }
	
	private BigDecimal getDecimalFromString(int field, String s) throws FieldException {
        try {
            return DecimalConverter.convert(s);
        } catch (final FieldConvertError e) {
            throw new FieldException(e.getMessage(), field);
        }
    }
	
	public BigDecimal getDecimal(int field) throws FieldNotFound, FieldException {
        return getDecimalFromString(field, getString(field));
    }
	
	public double getDouble(int field) throws FieldNotFound, FieldException {
        try {
            return DoubleConverter.convert(getString(field));
        } catch (final FieldConvertError e) {
            throw new FieldException(e.getMessage(), field);
        }
    }
	
	public boolean isSetField(int field) {
        return fields.containsKey(field);
    }
	
	public void removeField(int field) {
        fields.remove(field);
    }
}
