package quoteprice;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import quoteprice.exception.ConfigError;
import quoteprice.mina.EventHandlingStrategy;
import quoteprice.mina.acceptor.AbstractSocketAcceptor;

public class SocketAcceptor extends AbstractSocketAcceptor {
	private final AtomicBoolean isStarted = new AtomicBoolean(false);
    private final EventHandlingStrategy eventHandlingStrategy;
    
    public SocketAcceptor(Properties settings, SessionFactory sessionFactory, EventHandlingStrategy strategy) throws ConfigError{
    	super(settings, sessionFactory);
    	this.eventHandlingStrategy = strategy;
    }
    
    @Override
    public void start() throws ConfigError {
        initialize();
    }
    
    private void initialize() throws ConfigError {
        synchronized (isStarted) {
            if (isStarted.compareAndSet(false, true)) {
                startAcceptingConnections();
                eventHandlingStrategy.startHandling();
            }
        }
    }

    @Override
    public void stop(boolean forceDisconnect) {
        synchronized (isStarted) {
            if (isStarted.compareAndSet(true, false)) {
                try {
                    stopAcceptingConnections();
                } finally {
                    try {
                        eventHandlingStrategy.stopHandling(true);
                    } finally {
                    }
                }
            }
        }
    }

	@Override
	protected EventHandlingStrategy getEventHandlingStrategy() {
		return eventHandlingStrategy;
	}
}
