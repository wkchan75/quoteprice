package quoteprice;

public interface PricingEngine {

	double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity);
	
}
