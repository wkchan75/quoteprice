package quoteprice.exception;

public class ConfigError extends Exception {
	
	public ConfigError() {
        super();
    }

    public ConfigError(String message) {
        super(message);
    }

    public ConfigError(Throwable cause) {
        super(cause);
    }

    public ConfigError(String string, Throwable e) {
        super(string, e);
    }
}
