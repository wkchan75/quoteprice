package quoteprice.exception;

public class RuntimeError extends RuntimeException {

	public RuntimeError() {
        super();
    }

    public RuntimeError(String message) {
        super(message);
    }

    public RuntimeError(Throwable e) {
        super(e);
    }

    public RuntimeError(String message, Throwable cause) {
        super(message, cause);
    }
}
