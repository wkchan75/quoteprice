package quoteprice.exception;

public class FieldNotFound extends Exception{

	public FieldNotFound(int field) {
        super("Field was not found in message, field=" + field);
        this.field = field;
    }

    public FieldNotFound(String s) {
        super(s);
    }

    public int field;
}
