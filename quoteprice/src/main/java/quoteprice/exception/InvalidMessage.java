package quoteprice.exception;

import quoteprice.Message;

public class InvalidMessage extends Exception {
	
	Message quoteMessage;
	
	public InvalidMessage() {
        super();
    }

	public InvalidMessage(String errMsg) {
        super(errMsg);
    }
	
	public InvalidMessage(String errMsg, Message quoteMessage) {
        super(errMsg);
        this.quoteMessage = quoteMessage;
    }
	
	public InvalidMessage(String errMsg, Throwable cause, Message quoteMessage) {
        super(errMsg, cause);
        this.quoteMessage = quoteMessage;
    }
	
	public Message getQuoteMessage() {
        return quoteMessage;
    }
}
