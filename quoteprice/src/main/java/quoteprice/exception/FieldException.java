package quoteprice.exception;

public class FieldException extends Exception {
	
	private final int field;
	
	public FieldException(String msg, int field) {
        super(msg);
        this.field = field;
    }
	
	public int getField() {
        return field;
    }
}
