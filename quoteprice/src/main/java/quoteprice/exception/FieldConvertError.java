package quoteprice.exception;

public class FieldConvertError extends Exception {

	public FieldConvertError(String s) {
        super(s);
    }
}
