package quoteprice;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultReferencePriceSource implements ReferencePriceSource {

	private final ConcurrentHashMap<Integer, BigDecimal> instrumentPrices = new ConcurrentHashMap<>();
	
	public DefaultReferencePriceSource(ReferencePriceSourceListener listener){
		subscribe(listener);
	}
	
	@Override
	public double get(int securityId) {
		
		BigDecimal price = instrumentPrices.get(Integer.valueOf(securityId));
		if (price != null) {
			return price.doubleValue();
		}
		return 0;
	}

	@Override
	public void subscribe(ReferencePriceSourceListener listener) {
		listener.doSubscription(this);
	}
	
	public void set(int securityId, double value) {
		instrumentPrices.put(Integer.valueOf(securityId), BigDecimal.valueOf(value));
	}

}
