package quoteprice;

import quoteprice.field.Qty;
import quoteprice.field.SecId;
import quoteprice.field.Side;
import quoteprice.message.QuoteRequest;

public class MessageFactory {

	public static Message create(String msgType) {
		if (msgType == QuoteRequest.MSG_TYPE) {
			return new QuoteRequest(new SecId(), new Side(), new Qty());
		}
		return null;
	}
	
}
