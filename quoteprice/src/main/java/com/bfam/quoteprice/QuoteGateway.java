package com.bfam.quoteprice;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quoteprice.Gateway;
import quoteprice.Message;
import quoteprice.PricingEngine;
import quoteprice.ReferencePriceSource;
import quoteprice.SessionFactory;
import quoteprice.SocketAcceptor;
import quoteprice.StringField;
import quoteprice.exception.ConfigError;
import quoteprice.exception.FieldException;
import quoteprice.exception.FieldNotFound;
import quoteprice.field.Price;
import quoteprice.message.QuoteRequest;
import quoteprice.message.QuoteResponse;
import quoteprice.mina.EventHandlingStrategy;
import quoteprice.mina.SingleThreadedEventHandlingStrategy;

public class QuoteGateway implements Gateway {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private SocketAcceptor acceptor;
	private EventHandlingStrategy strategy;
	private SessionFactory sessionFactory;
	private PricingEngine quoteEngine;
	private ReferencePriceSource priceSource;
	
	public QuoteGateway(Properties settings, PricingEngine quoteEngine, ReferencePriceSource priceSource) throws ConfigError {
		sessionFactory = new SessionFactory(this);
		strategy = new SingleThreadedEventHandlingStrategy(-1);
		acceptor = new SocketAcceptor(settings, sessionFactory, strategy);
		this.quoteEngine = quoteEngine;
		this.priceSource = priceSource;
	}
	
	public void handleQuote() {
		try {
			acceptor.start();
		} catch (ConfigError e) {
			e.printStackTrace();
		}
	}
	
	public void onQuoteRequest(Message message) {
		QuoteRequest msg = (QuoteRequest) message;
		try {
			int secId = msg.getSecIdID();
			String side = msg.getSide();
			int qty = msg.getQty();
			log.info("Received QuoteRequest for SecurityID:{}, Side:{}, Quantity:{}", secId, side, qty);
			
			boolean buy = side.equalsIgnoreCase("BUY");
			double referencePrice = priceSource.get(secId);
			double price = this.quoteEngine.calculateQuotePrice(secId, referencePrice, buy, qty);
			//double price = onProcessQuote(secId, side, qty);
			onSendQuote(message, price);
		} catch (FieldNotFound e) {
			e.printStackTrace();
		} catch (FieldException e) {
			e.printStackTrace();
		}
	}
	
	//public abstract double onProcessQuote(int secId, String side, int qty);

	void onSendQuote(Message req, double value) {
		Price p = new Price(value);
		QuoteResponse msg = new QuoteResponse(p);
		try {
			msg.getField(44).setValue(Double.toString(value));
		} catch (FieldNotFound e) {
			e.printStackTrace();
		}
		msg.setSession(req.getSession());
		msg.setFieldOrder();
		sendQuote(msg);
	}
	
	public void sendQuote(Message message) {
		boolean result = message.getSession().send(message);
		try {
			StringField value = message.getField(44);
			if (result) {
				log.info("Quote has sent to client successfully! Price:{}", value.getObject());
			}
			else {
				log.error("Quote unable to send to client! Price:{}", value.getObject());
			}
			
		} catch (FieldNotFound e) {
			e.printStackTrace();
		}
	}
}
