package com.bfam.quoteprice;

import java.util.Properties;

import quoteprice.BloombergPriceSourceListener;
import quoteprice.DefaultReferencePriceSource;
import quoteprice.QuoteCalculationEngine;
import quoteprice.SessionFactory;
import quoteprice.exception.ConfigError;

public class QuoteManager extends QuoteGateway{
	
	Properties settings;
	//QuoteCalculationEngine pricing_engine = new QuoteCalculationEngine(new DefaultReferencePriceSource());
	
	public QuoteManager(Properties prop) throws ConfigError{
		super(	prop, 
				new QuoteCalculationEngine(),
				new DefaultReferencePriceSource(new BloombergPriceSourceListener()));
		settings = prop;
	}
	
	public void startProcess() throws ConfigError {
		handleQuote();
	}
	
}
