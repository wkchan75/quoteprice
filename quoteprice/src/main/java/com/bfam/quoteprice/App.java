package com.bfam.quoteprice;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import quoteprice.Message;
import quoteprice.MessageFactory;
import quoteprice.exception.ConfigError;
import quoteprice.exception.FieldNotFound;
import quoteprice.exception.InvalidMessage;
import quoteprice.message.QuoteRequest;


public class App 
{
	App(){
	}
	
	public void run(){
	
		Properties prop = new Properties();
		InputStream input = this.getClass().getClassLoader().getResourceAsStream("config.properties");
		
		try {
	    	prop.load(input);
	    	QuoteManager manager = new QuoteManager(prop);
	    	manager.startProcess();
	    	
    	} catch (IOException ex) {
    	    ex.printStackTrace();
    	} catch (ConfigError e) {
			e.printStackTrace();
		} finally {
    	    if (input != null) {
    	        try {
    	            input.close();
    	        } catch (IOException e) {
    	            e.printStackTrace();
    	        }
    	    }
    	}
	}
	
    public static void main( String[] args ) throws IOException
    {
    	App app = new App();
    	app.run();
    }
	
}
