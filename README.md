# Interview Exercise

Implement a server that responds to quote requests.

## Requirements

The server should accept TCP connections from one or more clients and respond to their requests.  Requests are sent on a 
single line, in the format of:

    {security ID} (BUY|SELL) {quantity}

Where `security ID` and `quantity` are integers.

For example:

    123 BUY 100

Is a request to buy 100 shares of security 123.

This should be responded to with a single line with a single numeric value representing the quoted price.

To calculate the quote price, two interfaces have been provided.

* `QuoteCalculationEngine` - to calculate the quote price based on a security, buy/sell indicator, requested
quantity and reference price.
* `ReferencePriceSource` - source of reference prices for the `QuoteCalculationEngine`.

The server should be capable of handling a large number of quote requests and be able to respond in a timely manner.

## Assumptions
- The server only accepting QuoteRequest message because there is no header to identify the message type in the message format.
- By design the server can accept multiple client connections, though I only tested single client connection in my testing.
- No checksum validation will be perform.
- There are only 3 fields will be in Quote Request message. The field separator should be a blank character i.e. ASCII (32). 
- There is no implementation for ReferencePrice Listener. Only 3 fixed reference prices will be populate by BloombergPriceSourceListener during startup through runnable method.
- By design, BloombergPriceSourceListener is the wrapper for listening market data or reference price. But there is no requirement to connect to external system in this test.
- Quote price is calculated base on reference price + 2% spread. I didn't use any pricing model to price the instrument as they are not enough parameters for pricing.
- The Quote response message only contains of Price field.
- The callback function "onQuoteRequest" will be called back from processMessage method of MessageSent event in SingleThreadedEventHandlingStrategy.
- Session will be created for each client connections for sending/receiving data from remote host.
- The server will be act as acceptor. and is listening for incoming connection via SocketAcceptor instance.
- No load test has been performed in this exercise.

## Testing

I have attached the test evidences in the project file. They can be viewed from Eclipse under "quoteprice" project.
Please note that Python has been used to simulate the remote client connection in my testing.

import socket
HOST = '127.0.0.1'  
PORT = 15000        
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(b'1234 SELL 25')
    print('Quote Response is for Sec ID(1234) is ' + s.recv(1024).decode('utf-8'))
    s.sendall(b'4567 SELL 13')
    print('Quote Response is for Sec ID(4567) is ' + s.recv(1024).decode('utf-8'))
    s.sendall(b'1000 SELL 50')    
    print('Quote Response is for Sec ID(1000) is ' + s.recv(1024).decode('utf-8'))

